import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';
import { CatalogService } from './features/catalog/services/catalog.service';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="home">home</button>
    <button routerLink="uikit">uikit</button>
    <button routerLink="uikit2">uikit2</button>
    <button routerLink="catalog">catalog</button>
    <button (click)="authService.signIn()">Login</button>
    <button appIsLogged (click)="authService.signOut()">Logout</button>
    {{authService.isLogged}}
    <hr>
    <router-outlet></router-outlet>
    
  `,
  styles: []
})
export class AppComponent {
  constructor(public authService: AuthService) {
  }

  render() {
    console.log('app component: render')
  }
}
