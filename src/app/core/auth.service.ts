import { Injectable } from '@angular/core';

export interface Auth {
  role: 'admin' | 'moderator'
  token: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data: Auth | null = null;

  signIn() {
    setTimeout(() => {
      this.data = {
        token: '123213',
        role: 'admin'
      }
    }, 400)
  }

  signOut() {
    this.data = null;
  }

  get isLogged(): boolean {
    return !!this.data;
  }
}
