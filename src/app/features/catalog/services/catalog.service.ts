import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  data: any = {}

  constructor() { }

  add(key: string, dt: any) {
    this.data[key] = dt;
    console.log(this.data)
  }
}
