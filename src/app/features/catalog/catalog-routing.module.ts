import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogComponent } from './catalog.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogComponent,
    children: [
      { path: 'products', loadChildren: () => import('./pages/products/products.module').then(m => m.ProductsModule) },
      { path: 'last', loadChildren: () => import('./pages/last-minute/last-minute.module').then(m => m.LastMinuteModule) },
      { path: 'offers', loadChildren: () => import('./pages/offers/offers.module').then(m => m.OffersModule) },
      { path: '', redirectTo: 'products'}
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
