import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs';
import { CatalogService } from '../../services/catalog.service';

@Component({
  selector: 'app-last-minute',
  template: `
    <form [formGroup]="form" >
      <input type="text"  formControlName="name">
      <div *ngIf="form.get('name')?.errors?.['cannotBeAAA']">non puo essere AAA</div>
      <input type="text"  formControlName="surname">
      <button [disabled]="form.invalid">SAVE</button>
    </form>
    
    {{form.get('name')?.valid}}
    {{form.get('name')?.errors | json}}
    
  `,
})
export class LastMinuteComponent {
  form: FormGroup;

  constructor(
    public catalogService: CatalogService,
    private fb: FormBuilder
  ) {

    this.form = fb.group({
      name: [
        '',
        [Validators.required, Validators.minLength(3), customValidator],
        // [UserValidators], async validators
      ],
      surname: '',
    })
    if (this.catalogService.data['last-minute']) {
      this.form.patchValue(this.catalogService.data['last-minute']);
    }

    this.form.valueChanges
      .pipe(
        debounceTime(1000)
      )
      .subscribe(value => {
        this.catalogService.add('last-minute', value)
      })

  }

  save(data: any) {
    this.catalogService.add('offers', data)
  }

}


function customValidator(c: FormControl): ValidationErrors | null {
  console.log(c.value)
  if(c.value === 'aaa') {
    return {
      cannotBeAAA: true
    }
  }
  return null
}
