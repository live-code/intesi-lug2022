import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { debounceTime, fromEvent } from 'rxjs';
import { CatalogService } from '../../services/catalog.service';

@Component({
  selector: 'app-products',
  template: `
    <h1>Products Store</h1>
    <input type="text"  #searchInput>
    <app-product-search></app-product-search>
    <app-product-list></app-product-list>
    
    {{catalogService.data | json}}
  `,
  styles: [
  ]
})
export class ProductsComponent implements AfterViewInit {
  @ViewChild('searchInput') searchInput!: ElementRef<HTMLInputElement>;

  constructor(public catalogService: CatalogService) {}

  ngAfterViewInit(): void {
     this.searchInput.nativeElement.value = this.catalogService.data.search || ''
    // --------------o-o-ooooo-oo-oo-------o-->
    // debounceTime(----)
    // ---------------------------------o
    fromEvent(this.searchInput.nativeElement, 'input')
      .pipe(
        debounceTime(1000)
      )
      .subscribe(e => {
        this.catalogService.add('search', (e.target as HTMLInputElement).value)
      })
  }

}
