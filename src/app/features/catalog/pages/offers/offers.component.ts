import { Component, OnInit } from '@angular/core';
import { CatalogService } from '../../services/catalog.service';

@Component({
  selector: 'app-offers',
  template: `
    <p>
      offers works!
    </p>
    <form #f="ngForm" (submit)="save(f.value)">
      <input type="text" name="username" ngModel>
      <input type="text" name="password" ngModel>
      <button type="submit">ADD</button>
    </form>
    
    <hr>
    <pre>{{catalogService.data | json}}</pre>
  `,
  styles: [
  ]
})
export class OffersComponent {
  constructor(public catalogService: CatalogService) { }

  save(data: any) {
    this.catalogService.add('offers', data)
  }
}
