import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';

import { OffersRoutingModule } from './offers-routing.module';
import { OffersComponent } from './offers.component';


@NgModule({
  declarations: [
    OffersComponent
  ],
  imports: [
    CommonModule,
    OffersRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class OffersModule { }
