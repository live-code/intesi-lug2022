import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog',
  template: `
    <button routerLink="offers">offers</button>
    <button routerLink="products">products</button>
    <button routerLink="last">last-minute</button>
    <hr>
   
    <router-outlet></router-outlet>
    
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
