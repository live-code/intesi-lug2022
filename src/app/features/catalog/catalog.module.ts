import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { CatalogService } from './services/catalog.service';


@NgModule({
  declarations: [
    CatalogComponent,
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule
  ],
})
export class CatalogModule { }
