import { ChangeDetectionStrategy, Component, OnInit, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import { PanelComponent } from '../../shared/components/panel/panel.component';

@Component({
  selector: 'app-uikit',
  template: `
    <div 
      [appBg]="value"
      [appPad]="1"
      [appMargin]="marginValue"
    >DIRECTIVE EXAMPLE</div>
    <span [appMargin]="1">123</span>
    
    <button appUrl="http://www.google.com">
      google
    </button>
    <hr>
    
    <div appTooltip="trieste">show map</div>
      
    <hr>
   <div appIsLogged>ADMIN FEATURE</div>
    <button (click)="marginValue = marginValue + 1">+ margin</button>
    <button (click)="value = 'red'">red</button>
    <button (click)="value = 'pink'">pink</button>
    
    <hr>
    
    <ng-template #tpl>
      ciao
    </ng-template>
    
    <div *appPad="1">padding</div>
  `,
})
export class UikitComponent implements OnInit {
  @ViewChild('tpl') tpl!: TemplateRef<any>
  value = 'blue'
  marginValue = 0;

  constructor(
    public authSrv: AuthService,
    private view: ViewContainerRef
  ) {
    setTimeout(() => {
      this.view.createEmbeddedView(this.tpl)
    }, 1000)
  }

  ngOnInit(): void {
  }

}
