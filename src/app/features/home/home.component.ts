import { Component, OnInit } from '@angular/core';

export interface News {
  id: number;
  name: string;
}

@Component({
  selector: 'app-home',
  template: `
    <h1>
      home example: onPush
    </h1>
  
    <app-home-add
      [data]="data"
      (add)="addNews($event)"></app-home-add>
    <app-home-list
      [visibility]="value"
      (deleteNews)="deleteHandler($event)"
      [data]="data"></app-home-list>
    
    {{data?.length}}
  
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  value = true;
  data: News[] | null = null;

  constructor() {
    setTimeout(() => {
      this.data = [
        { id: 1, name: 'A'},
        { id: 2, name: 'B'},
        { id: 3, name: 'C'},
      ]
    }, 400)
  }

  addNews(newsTitle: string) {
    /*this.data?.push({ id: Math.random(), name: newsTitle})
    this.value = !this.value*/
    const nuovaNews = { id: Math.random(), name: newsTitle};
    if (this.data) {
      const newData = JSON.parse(JSON.stringify(this.data))
      newData.push(nuovaNews)
      this.data = newData
    }
  }
  ngOnInit(): void {
  }

  render() {
    console.log('home: render')
  }

  deleteHandler(id: number) {
    setTimeout(() => {
      const index = this.data!.findIndex(d => d.id === id);
      if (index !== -1) {
       // this.data!.splice(index!, 1)
        this.data = this.data!.filter(d => d.id !== id)
      }
    }, 1000)

  }
}
