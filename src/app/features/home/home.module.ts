import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { CatalogService } from '../catalog/services/catalog.service';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeListComponent } from './components/home-list.component';
import { HomeListItemComponent } from './components/home-list-item.component';
import { HomeSearchComponent } from './components/home-search.component';


@NgModule({
  declarations: [
    HomeComponent,
    HomeListComponent,
    HomeListItemComponent,
    HomeSearchComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
  ]
})
export class HomeModule { }
