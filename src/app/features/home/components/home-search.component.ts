import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-home-add',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p>
      ADD NEWS
    </p>
    <input type="text" [formControl]="input">
    <button
      (click)="add.emit(input.value)"
      [disabled]="input.invalid">ADD {{data?.length}}</button>
    
    {{render()}}
  `,
  styles: [
  ]
})
export class HomeSearchComponent {
  @Output() add = new EventEmitter<string>()
  @Input() data!: any[] | null;
  input = new FormControl('', Validators.required)

  render() {
    console.log('home search: render')
  }
}


function cloned(val: any) {
  return JSON.parse(JSON.stringify(val))
}
