import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AuthService } from '../../../core/auth.service';
import { CatalogService } from '../../catalog/services/catalog.service';
import { News } from '../home.component';

@Component({
  selector: 'app-home-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p>
      home-list works!
    </p>
    <li *ngFor="let d of data">
      {{d.name}}
      <button  (click)="deleteNews.emit(d.id)" >Delete</button>
    </li>
    
    {{render()}}
  `,
  styles: [
  ]
})
export class HomeListComponent implements OnInit {
  @Input() data: News[] | null = null;
  @Input() visibility: boolean = false;
  @Output() deleteNews = new EventEmitter<number>()

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

  render() {
    console.log('home list: render')
  }
}
