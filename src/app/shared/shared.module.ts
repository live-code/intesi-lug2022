import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogService } from '../features/catalog/services/catalog.service';
import { PanelComponent } from './components/panel/panel.component';
import { BgDirective } from './directives/bg.directive';
import { PadDirective } from './directives/pad.directive';
import { ColorDirective } from './directives/color.directive';
import { IsLoggedDirective } from './directives/is-logged.directive';
import { MarginDirective } from './directives/margin.directive';
import { TooltipDirective } from './directives/tooltip.directive';
import { UrlDirective } from './directives/url.directive';
import { MapquestDirective } from './components/mapquest.directive';
import { MapquestComponent } from './components/mapquest/mapquest.component';



@NgModule({
  declarations: [
    PanelComponent,
    BgDirective,
    PadDirective,
    ColorDirective,
    IsLoggedDirective,
    MarginDirective,
    TooltipDirective,
    UrlDirective,
    MapquestDirective,
    MapquestComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PanelComponent,
    BgDirective,
    PadDirective,
    ColorDirective,
    IsLoggedDirective,
    MarginDirective,
    TooltipDirective,
    UrlDirective,
    MapquestDirective,
    MapquestComponent,
  ],
  providers: [

  ]
})
export class SharedModule {
  static forRoot(config: string): ModuleWithProviders<any> {
    return {
      ngModule: SharedModule,
     /* providers: [
        {
          provide: CatalogService,
          useFactory: () => {
            return new CatalogService(config)
          }
        }
      ]*/
    }
  }
}
