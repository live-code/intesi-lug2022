import { Directive, HostBinding } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIsLogged]'
})
export class IsLoggedDirective {

  @HostBinding() get hidden() {
    return !this.authService.isLogged
  }

  constructor(private authService: AuthService) {
  }
}
