import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appMargin]'
})
export class MarginDirective {
  @Input() set appMargin(val: number) {
    // this.el.nativeElement.style.margin = val + 'px';
    this.renderer.setStyle(
      this.el.nativeElement,
      'margin',
      val + 'px'
    )
  }

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) {

  }

}

