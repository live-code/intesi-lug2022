import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appPad]'
})
export class PadDirective {
  @Input() appPad: 1 | 2 | 3 | 0  = 0;

  @HostBinding('style.padding') get padding() {
    return (+this.appPad * 10) + 'px';
  }
  constructor() { }

}
