import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBg]'
})
export class BgDirective {
  @Input() appBg: string = ''

  @HostBinding('style.background-color') get bg() {
    return this.appBg;
  }
}
