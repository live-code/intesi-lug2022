import { Directive, ElementRef, HostListener, Input, ViewContainerRef } from '@angular/core';
import { MapquestComponent } from '../components/mapquest/mapquest.component';
import { PanelComponent } from '../components/panel/panel.component';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective {
  @Input() appTooltip = ''
  @HostListener('mouseover')
  over() {
    const ref = this.view.createComponent(MapquestComponent)
    ref.instance.city = this.appTooltip;
  }

  @HostListener('mouseout')
  out() {
    this.view.clear();
  }

  constructor(
    private el: ElementRef,
    private view: ViewContainerRef
  ) {

  }

}
