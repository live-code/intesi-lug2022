import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-mapquest',
  template: `
    <img width="100" [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + this.city + '&size=600,400'" alt="">
  `,
  styles: [
  ]
})
export class MapquestComponent {
  @Input() city: string = ''

}
