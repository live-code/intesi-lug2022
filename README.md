• Base \ DOM
V • ElementRef e nativeElement
• Pipe e Direttive custom strutturali
• Architettura
• Binding dati, Gestione Stato e Side Effect
V • Immutabilità? Perchè è utile e quando?

• RxJS
• Approfondimento su Subject: come funzionano, potenzialità e le differenze tra le quattro tipologie: Subject, BehaviorSubject, AsyncSubject, ReplaySubject.
• Gestire lo stato applicativo e creare architetture Angular con RxJS, l’utilizzo di Subject sfruttando il motore di dependency injection

-----------
- NEXT
- direttive strutturali e Rxjs
- pipe
- rxjs
